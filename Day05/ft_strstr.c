/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strstr.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: sasiedu <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/04/01 20:00:14 by sasiedu           #+#    #+#             */
/*   Updated: 2016/04/01 22:30:54 by sasiedu          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

char	*ft_strstr(char *str, char *to_find)
{
	int	i;
	int	j;

	i = 0;
	if (!to_find[0])
		return (str);
	while (str[i] != '\0')
	{
		j = 0;
		if (to_find[j] == str[i])
		{
			while (to_find[j] == str[i])
			{
				i++;
				j++;
			}
			if (to_find[j] == '\0')
                                        return (to_find);
		}
		i++;
	}
	return (NULL);
}
