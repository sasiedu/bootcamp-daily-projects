/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strcapitalize.c                                 :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: sasiedu <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/04/01 20:00:14 by sasiedu           #+#    #+#             */
/*   Updated: 2016/04/01 22:30:54 by sasiedu          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

char	*ft_strcapitalize(char *str)
{
	int	i;

	i = 0;
	while (str[i])
	{
		if (str[0] >= 'a' && str[i] <= 'z')
			str[0] = str[0] - 32;
		else if (str[i - 1] < 97 || str[i - 1] > 122)
		{
			if (str[i - 1] < 65 || str[i - 1] > 90)
			{
				if (str[i - 1] < 48 || str[i - 1] > 57)
				{
					if (str[i] >= 'a' && str[i] <= 'z')
						str[i] = str[i] - 32;
				}
			}
		}
		else
			str[i] = str[i];
		i++;
	}
	return (str);
}
