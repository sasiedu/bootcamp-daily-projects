
#include	<stdio.h>

int	ft_atoi(char *str)
{
	int	i;
	int	res;
	int	neg;

	i = 0;
	res = 0;
	neg = 1;
	if (str[i] == '-')
	{
		neg = -1;
		i++;
	}
	while (str[i] != '\0')
	{
		res = res * 10 + str[i] - '0';
		i++;
	}
	return (neg * res);
}

int	main(int argc, char **argv)
{
	if (argc == 2)
	{
		printf("%d""\n", (ft_atoi(argv[1])));
	}
	return (0);
}
