/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_sort_integer_table.c                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: sasiedu <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/04/04 22:23:14 by sasiedu           #+#    #+#             */
/*   Updated: 2016/04/04 22:35:54 by sasiedu          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include	<stdio.h>

void	ft_sort_integer_table(int *tab, int size)
{
	int	i;
	int	temp;

	i = 0;
	while (i < size)
	{
		if (tab[i + 1] > tab[i])
			i++;
		else
			while (tab[i + 1] < tab[i])
			{
				temp = tab[i];
				tab[i] = tab[i + 1];
				tab[i + 1] = temp;
				i--;
			}
	}
}

int	main()
{
	int	tab[] = {5,3,1,2,6};

	printf("%s""\n", (ft_sort_integer_table(tab, 5)));
	return (0);
}
