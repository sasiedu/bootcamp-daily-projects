/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_atoi.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: sasiedu <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/04/04 22:08:14 by sasiedu           #+#    #+#             */
/*   Updated: 2016/04/04 22:14:54 by sasiedu          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

int	ft_atoi(char *str)
{
	if (str == NULL)
		return (0);

	int	res;
	int	sign;
	int	i;

	res = 0;
	sign = 1;
	i = 0;

	/* update sign if its negative */
	if (str[0] == '-')
	{
		sign = -1;
		i++; /* move to the next digit */
	}

	/* iterate through all digits of input string and update result */
	while (str[i] != '\0')
	{
		res = res * 10 + str[i] - '0';
		i++;
	}
	/* result with sign */
	return (sign * res);
}
