/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strrev.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: sasiedu <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/04/04 15:14:14 by sasiedu           #+#    #+#             */
/*   Updated: 2016/04/04 15:20:54 by sasiedu          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

char	*ft_strrev(char *str)
{
	char	dest;
	int	i;
	int	j;

	i = ft_strlen(str) - 1;
	j = 0;
	while (i > j)
	{
		dest = str[i];
		str[i] = str[j];
		str[j] = dest;
		i--;
		j++;
	}
	return (str);
}
