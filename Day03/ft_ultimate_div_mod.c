/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_ultimate_div_mod.c                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: sasiedu <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/04/04 12:00:14 by sasiedu           #+#    #+#             */
/*   Updated: 2016/04/04 12:15:54 by sasiedu          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

void	ft_putnbr(int nb);
void	ft_putchar(char c);

void	ft_ultimate_div_mod(int *a, int *b)
{
	int	div;
	int	mod;

	div = (*a / *b);
	mod = (*a % *b);
	a = &div;
	b = &mod;
	ft_putnbr(*a);
	ft_putchar('\n');
	ft_putnbr(*b);
	ft_putchar('\n');
}

int	main()
{
	int	*a;
	int	*b;
	int	c;
	int	d;

	c = 3;
	d = 5;
	a = &c;
	b = &d;
	ft_ultimate_div_mod(a, b);
	return (0);
}
